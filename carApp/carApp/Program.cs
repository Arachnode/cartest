﻿using System;
using System.Collections.Generic;

namespace carApp
{
    class Program
    {
        class Car
        {
            public string Make { get; set; }
            public string Model { get; set; }
            public string Color { get; set; }
            public int Year { get; set; }
            public decimal Price { get; set; }
            public double TCCR { get; set; }
            public int MPG { get; set; }

            public Car()
            {
                Console.WriteLine("Car Created");
            }
        }

        // This class will be used to hold information, as well as any functionality relating to that information
        class CarDealer
        {
            // our list of cars. I chose the List type because it's easy to sort and query. When you make a List,
            // you just specify the type that the list will have in it, pretty simple.
            public List<Car> Cars;
            
            // constructor for our car dealer
            public CarDealer()
            {
                // we want to initialize our list of cars in the constructor. We don't want to have to worry about this
                // list being null later on, and there's no reason to reinitialize it later, so we should only have to 
                // do this once when the car dealer is created.
                CreateInventory();


                // just for fun, let's greet the customer ;)
                Console.WriteLine("Welcome to the Car Hut, home of... Cars... Let me tell you about our available stock!");
            }

            // here we have the opportunity to take care of two tasks at once. Creating a car, and adding it to the list.
            // alternatively we could have made a CreateCar method that would take these arguments, and then this AddCar
            // method would only need to take a Car object. I don't feel like there's enough work in either task to justify
            // separating out the functionality though.
            public Car AddCar(string make, string model, string color, int year, decimal price, double tccr, int mpg)
            {
                // rather than just creating the 
                var car = new Car()
                {
                    Make = make,
                    Model = model,
                    Color = color,
                    Year = year,
                    Price = price,
                    TCCR = tccr,
                    MPG = mpg
                };
                Cars.Add(car);
                return car;
            }

            // this helper function takes care of populating our list of cars by calling our AddCar method.
            private void CreateInventory()
            {
                // first we will assign this member a new list object that we can populate
                Cars = new List<Car>();

                // now we can call our AddCar method to populate that list
                AddCar("Honda", "CRV", "Green", 2016, 23845, 8, 20);
                AddCar("Honda", "CRV", "Brown", 2013, 15800, 8, 35);
                AddCar("Ford", "Escape", "Red", 2005, 106543, 5, 18);
                AddCar("Subaru", "Forrester", "White", 2014, 35000, 8.4, 30);
            }

            // this method will actually be called outside of the car dealer class
            public void ListVehiclesByYear()
            {
                // although it's not entirely necessary, I am making a copy of our list of cars, so that when we sort
                // the list, it won't touch the original list. It could be argued that this is a waste of resources
                // given the scope of this project, but I think it's good practice to avoid messing with original data
                var sortedCars = new List<Car>(Cars);
                
                // now we get to some tricky stuff. We are calling sort on the list, and sort takes a function as an
                // argument. So we have a little fancy function ()=>{} with two arguments (which is what the list's
                // sort method provides apparently) which I have named x and y, for no reason. The values of x and y
                // are going to be two cars in our list. Since each of them are car objects, we have access to their
                // properties, and can compare them by calling .CompareTo.. which I feel is pretty self explanitory.
                // The result of CompareTo should be 1, 0, or -1 for greater than, equal to, or less than, respecitvely.
                // I could be wrong, but I think that's how it works, feel free to look it up! That resulting integer
                // value tells the Sort method if x should go before y, or vice versa.
                sortedCars.Sort((x, y) => x.Year.CompareTo(y.Year));

                // if you would like the list to be reversed, it's as simple as calling .Reverse()
                // sortedCars.Reverse();

                // now that we have our list sorted the way we want it, we can print the result
                Console.Write("Here's our inventory by year:\n");
                foreach(Car car in sortedCars){
                    Console.WriteLine($"Make: {car.Make} Model: {car.Model} Year: {car.Year}");
                }


                // if you would like the list to be reversed, it's as simple as calling .Reverse()
                Console.Write("Oh I'm sorry, you probably are more interested in newer vehicles:\n");
                sortedCars.Reverse();
                foreach (Car car in sortedCars)
                {
                    Console.WriteLine($"Make: {car.Make} Model: {car.Model} Year: {car.Year}");
                }
            }

            // like the function above, but let's get fancy
            public void ListVehiclesByBestDeal()
            {
                var sortedCars = new List<Car>(Cars);
                // this time let's add some curly braces so we can have multiple lines of code that define this comparison
                sortedCars.Sort((x, y) => {
                    // you can do some sort of equation here to determine the worth of each car
                    // this equation is nonsense. I don't know how you want to determine worth ::shrugs::
                    var xWorth = x.Price  * (x.Year / x.MPG);
                    var yWorth = y.Price  * (y.Year / y.MPG);

                    // since we are in a curly brace scope, we need to return the value to sort of collapse all of this
                    // in to a usable value for the sort method.
                    return xWorth.CompareTo(yWorth);

                });


                // now we can list the cars
                Console.Write("Here is our inventory in order of overall value (in my opinion):\n");
                foreach (Car car in sortedCars)
                {
                    Console.WriteLine($"Make: {car.Make} Model: {car.Model} Year: {car.Year} MPG: {car.MPG} Rating: {car.TCCR} Price: {car.Price}");
                }
            }


        }

        static void Main(string[] args)
        {
            // now it's clear sailing. We can create our dealer
            var dealership = new CarDealer();
            // and now we can start calling our output methods for our car dealer class
            dealership.ListVehiclesByYear();
            dealership.ListVehiclesByBestDeal();
            // now rinse and repeat for the additional functionality you need. Also make sure you actually use the
            // real data they provided... 
            Console.ReadLine();
        }
    }
}
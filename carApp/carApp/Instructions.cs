﻿using System;
using System.Collections.Generic;
using System.Text;

namespace carApp
{
    class Instructions
    {
        //git clone https://Arachnode@bitbucket.org/Arachnode/cartest.git

        ///****************Car Comparison********************/

        //Data:
        //Make Model       Color Year    Price TCC Rating Hwy MPG
        //Honda       CRV Green 	2016 	$23,845		8				33
        //Ford Escape      Red 	2017 	$23,590		7.8				32
        //Hyundai Sante Fe Grey 	2016 	$24,950		8				27
        //Mazda CX-5 		Red 	2017 	$21,795		8				35
        //Subaru Forester    Blue	2016 	$22,395		8.4				32


        //Create tool based on this sample data that will give the following:
        //1) A function to calculate newest vehicles in order
        //2) A function to calculate alphabetized List of vehicles
        //3) A function to calculate ordered List of Vehicles by Price
        //4) A function to calculate the best value
        //5) A function to calculate full consumption for a given distance
        //6) A function to return a random car from the list
        //7) A function to return average MPG by year
    }
}
